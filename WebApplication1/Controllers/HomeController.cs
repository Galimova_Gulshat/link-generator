﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Data;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly DatabaseContext _context;

        public HomeController(DatabaseContext context) => _context = context;
        
        public IActionResult Index()
        {
            return View(_context.Links);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Show(string password, int id)
        {
            var link = _context.Links.FirstOrDefault(l => l.Id == id && l.Password == password);
            if (link == default(Link))
                return RedirectToAction("Index");
            return View(link);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(string fullLink, string shortLink, string password)
        {
            var shortL = string.Empty;
            if (shortLink != null)
            {
                if (_context.Links.FirstOrDefault(l => l.ShortLink == shortLink) != default(Link))
                {
                    ModelState.AddModelError("", "Подобная ссылка существует");
                    return View();
                }
                shortL = shortLink;
            }
            else
                shortL = GetRandomLink(fullLink);
            _context.Links.Add(new Link {FullLink = fullLink, UserEmail = User.Identity.Name, ShortLink = shortL, Password = password});
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
        
        private string GetRandomLink(string alphabet)
        {
            Random rnd = new Random();
            var length = rnd.Next(5, 10);
            StringBuilder sb = new StringBuilder(length-1);
            sb.Append("https://");
            int Position = 0;
            for (int i = 0; i < length; i++)
            {
                Position = rnd.Next(0, alphabet.Length-1);
                sb.Append(alphabet[Position]);
                if (i == 5)
                    sb.Append("/");
            }
            return sb.ToString();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}