namespace WebApplication1.Models
{
    public class Link
    {
        public int Id { get; set; }
        public string ShortLink { get; set; }
        public string FullLink { get; set; }
        public string UserEmail { get; set; }
        
        public string Password { get; set; }
    }
}